package com.example.uas_gedor_180030183;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class AdminActivity extends AppCompatActivity {
    Preferences preferences;
    TextView user_login_show;
    Button hotel,user,logout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        preferences = new Preferences(this);
        hotel = (Button) findViewById(R.id.btn_hotel);
        user = (Button) findViewById(R.id.btn_user);
        logout = (Button) findViewById(R.id.btn_logout);

        user_login_show = findViewById(R.id.user_login_show);
        user_login_show.setText(preferences.getNama());

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferences.clear();
                startActivity(new Intent(AdminActivity.this,LoginActivity.class));
            }
        });

        hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdminActivity.this,HotelActivity.class));
            }
        });
    }
}
