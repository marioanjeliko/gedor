package com.example.uas_gedor_180030183;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class Model extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "gedor.db";
    private static final int DATABASE_VERSION = 1;
    public Model(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    public void createTable(SQLiteDatabase db) {
        db.execSQL("create table if not exists user(id integer primary key, nama text , username text , password text, level text);");
        db.execSQL("create table  if not exists hotel(id integer primary key, nama text , location text , price text, star text, foto text);");
        db.execSQL("create table  if not exists biodata(id integer primary key, nama text , nim text,kelas text);");
    }

    public void generateData(SQLiteDatabase db) {
        ContentValues cv = new ContentValues();
        cv.put("nama","Mario Anjeliko");
        cv.put("username","admin");
        cv.put("password","admin");
        cv.put("level","admin");
        db.insert("user",null,cv);

        cv.put("nama","Mario Anjeliko");
        cv.put("nim","180030183");
        cv.put("kelas","BG183");
        db.insert("biodata",null,cv);


    }

    public void generateHotel(SQLiteDatabase db) {
        ContentValues cv = new ContentValues();
        cv.put("nama","tes hotell");
        cv.put("location","1233333");
        cv.put("price","15000");
        cv.put("star","4");
        cv.put("foto","tess");
        db.insert("hotel",null,cv);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
    }
    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub

    }
}
