package com.example.uas_gedor_180030183;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    Button login,reset;
    EditText username,password;
    Model user;
    SQLiteDatabase db;
    Cursor cursor,tes;
    Preferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        preferences = new Preferences(this);

        user = new Model(this);
        db = user.getWritableDatabase();
        user.createTable(db);

        cursor = db.rawQuery("SELECT * FROM user where level='admin'",null);
        if (!cursor.moveToNext())
            user.generateData(db);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        login = (Button) findViewById(R.id.login);
        reset = (Button) findViewById(R.id.reset);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (username.getText().toString().trim().equalsIgnoreCase("")) {
                    username.setError("Username Masih Kosong !");
                } else if (password.getText().toString().equalsIgnoreCase("")) {
                    password.setError("Password Masih Kosong !");
                } else {

                    String[] columns = {"*"};
                    db = user.getReadableDatabase();
                    String selection = "username" + "=?" + " and " + "password" + "=?";
                    String[] selectionArgs = {username.getText().toString(), password.getText().toString()};
                    Cursor cursor = db.query("user", columns, selection, selectionArgs, null, null, null);
                    int count = cursor.getCount();
                    cursor.moveToFirst();
                    if (count > 0) {
                        preferences.saveSPString(preferences.SP_USERNAME, "tess");
                        preferences.saveSPString(preferences.SP_NAMA, cursor.getString(1));
                        preferences.saveSPString(preferences.SP_LEVEL, cursor.getString(4));
                        Toast.makeText(getApplicationContext(), "Login Berhasil ! Selamat Datang "+cursor.getString(1), Toast.LENGTH_SHORT).show();

                        if(preferences.getLevel().equals("admin"))
                            startActivity(new Intent(LoginActivity.this, AdminActivity.class));
                         else
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));

                    } else {
                        Toast.makeText(getApplicationContext(), "Username / Password Salah !", Toast.LENGTH_SHORT).show();
                    }
                    cursor.close();
                    db.close();
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username.setText(null);
                password.setText(null);
            }
        });
    }
}
