package com.example.uas_gedor_180030183;

import java.util.ArrayList;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HotelAdapter extends ArrayAdapter<String> {

    int groupid;
    ArrayList<String> records;
    Context context;

    public HotelAdapter(Context context, int vg, int id, ArrayList<String> records){
        super(context,vg, id, records);
        this.context=context;
        groupid=vg;
        this.records=records;

    }
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(groupid, parent, false);

        String[] row_items=records.get(position).split("__");
        TextView id= (TextView) itemView.findViewById(R.id.text_id);
        id.setText(row_items[0]);
        TextView judul= (TextView) itemView.findViewById(R.id.text_judul);
        judul.setText(row_items[1]);
        TextView price= (TextView) itemView.findViewById(R.id.text_price);
        price.setText(row_items[3]);
        TextView kelas= (TextView) itemView.findViewById(R.id.text_kelas);
        kelas.setText(getIntToStar(Integer.parseInt(row_items[4])));
        ImageView foto= itemView.findViewById(R.id.text_img);
        foto.setImageURI(Uri.parse(row_items[5]));
        return itemView;
    }

    private String getIntToStar(int starCount) {
        String fillStar = "\u2605";
        String blankStar = "\u2606";
        String star = "";

        for (int i = 0; i < starCount; i++) {
            star = star.concat(" " + fillStar);
        }
        for (int j = (5 - starCount); j > 0; j--) {
            star = star.concat(" " + blankStar);
        }
        return star;
    }

}