package com.example.uas_gedor_180030183;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    public static final String SP_GEDOR = "spGEDOR";
    public static final String SP_ID = "spId";
    public static final  String SP_USERNAME = "spUsername";
    public static  final  String SP_NAMA ="spNama";
    public static  final  String SP_LEVEL ="spLevel";

    public static final String SP_SUDAH_LOGIN = "spSudahLogin";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    public Preferences(Context context){
        sp = context.getSharedPreferences(SP_GEDOR, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void saveSPString(String keySP, String value){
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPInt(String keySP, int value){
        spEditor.putInt(keySP, value);
        spEditor.commit();
    }

    public void saveSPBoolean(String keySP, boolean value){
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public String getID(){
        return sp.getString(SP_ID, null);
    }
    public String getUsername() { return  sp.getString(SP_USERNAME,null);}
    public String getNama(){
        return sp.getString(SP_NAMA, null);
    }
    public String getLevel(){
        return sp.getString(SP_LEVEL, null);
    }

    public Boolean getSPSudahLogin(){
        return sp.getBoolean(SP_SUDAH_LOGIN, false);
    }

    public void clear() {
        spEditor.clear().commit();
    }
}
