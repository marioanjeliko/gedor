package com.example.uas_gedor_180030183;

import android.Manifest;
import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.uas_gedor_180030183.data.AppDatabase;
import com.example.uas_gedor_180030183.data.entity.Hotel;
import com.example.uas_gedor_180030183.helper.ImageFilePath;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import io.reactivex.CompletableObserver;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.List;

public class HotelAdd extends AppCompatActivity {
    Button submit_add,reset_add,foto;
    EditText nama,location,star,price;
    private static final int PICK_IMAGE = 99;
    SQLiteDatabase db;
    AppDatabase mAppDatabase;
    String imgPath;
    Model hotel;
    ImageView imgview;
    private Uri selectedImageUri;
    Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_hotel_add);

        Dexter.withContext(HotelAdd.this)
                .withPermissions(
                    permission.READ_EXTERNAL_STORAGE
                    ,permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener(){

            @Override
            public void onPermissionsChecked(final MultiplePermissionsReport multiplePermissionsReport) {

            }

            @Override
            public void onPermissionRationaleShouldBeShown(final List<PermissionRequest> list,
                    final PermissionToken permissionToken) {
                    permissionToken.continuePermissionRequest();
            }
        }).check();

        hotel = new Model(this);
        db = hotel.getWritableDatabase();
        nama = (EditText) findViewById(R.id.add_nama);
        location = (EditText) findViewById(R.id.add_location) ;
        star = (EditText) findViewById(R.id.add_star);
        price = (EditText) findViewById(R.id.add_price);
        submit_add = (Button) findViewById(R.id.submit_add);
        reset_add = (Button) findViewById(R.id.reset_add);
        foto = (Button) findViewById(R.id.add_foto);
        reset_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nama.setText(null);
                price.setText(null);
                location.setText(null);
                star.setText(null);
            }
        });

        mAppDatabase = AppDatabase.getInstance(this);

       submit_add.setOnClickListener(new View.OnClickListener() {
           @SuppressLint("CheckResult")
           @Override
           public void onClick(View v) {
//               cursor = db.rawQuery("SELECT * FROM hotel",null);
//               Toast.makeText(getApplicationContext(),cursor.getCount(),Toast.LENGTH_SHORT).show();
               if (nama.getText().toString().trim().equalsIgnoreCase("")) {
                   nama.setError("Nama masih kosong !");
               } else if (location.getText().toString().trim().equalsIgnoreCase("")) {
                   location.setError("Lokasi masih kosong !");
               } else if (star.getText().toString().trim().equalsIgnoreCase("")) {
                   star.setError("Kelas hotel masih kosong !");
               } else if (price.getText().toString().trim().equalsIgnoreCase("")) {
                   price.setError("Harga masih kosong !");
               } else {
                   final Hotel hotel = new Hotel(
                           nama.getText().toString(),
                           location.getText().toString(),
                           price.getText().toString(),
                           star.getText().toString(),
                           imgPath
                   );

                   mAppDatabase.hotelDao().insertHotel(hotel)
                           .observeOn(AndroidSchedulers.mainThread())
                           .subscribeOn(Schedulers.io())
                           .subscribeWith(new CompletableObserver() {
                               @Override
                               public void onSubscribe(final Disposable d) {
                                   Log.d("DATA_GUE","OnSubscribe" +d);
                               }

                               @Override
                               public void onComplete() {
                                   Log.d("DATA_GUE","OnComplete");
                               }

                               @Override
                               public void onError(final Throwable e) {
                                   Log.d("DATA_GUE","OnError" +e.getLocalizedMessage());
                               }
                           });

//                   get list of hotel
                   mAppDatabase.hotelDao()
                           .getListHotel()
                           .observeOn(AndroidSchedulers.mainThread())
                           .subscribeOn(Schedulers.io())
                           .subscribeWith(new DisposableSingleObserver<List<Hotel>>() {
                           @Override
                           public void onSuccess(final List<Hotel> hotels) {
                               Log.d("DATA_GUE","DATA "+hotels.toString());
                           }

                           @Override
                           public void onError(final Throwable e) {
                               Log.d("DATA_GUE","DATA "+e.getLocalizedMessage());
                           }
                       });
//                try {
//                db.execSQL("INSERT INTO hotel(nama , location , price, star, foto) VALUES('"+nama+"','"+location+"','"+price+"','"+star+"','tess')");
//                    Toast.makeText(getApplicationContext(),"Berhasil !",Toast.LENGTH_SHORT).show();
//                } catch (SQLException e) {
//                    Toast.makeText(getApplicationContext(),"QUERY ERROR",Toast.LENGTH_SHORT).show();
//                }
               }
           }
       });
        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageFromGallery();
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();


            imgPath = ImageFilePath.getPath(HotelAdd.this, data.getData());
//                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());

            Log.i("DATA_GUE", "onActivityResult: file path : " + imgPath);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                ImageView imageView = (ImageView) findViewById(R.id.ivImage);
                imageView.setImageBitmap(bitmap);
                imageView.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
        }
    }

    public void selectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                PICK_IMAGE);
    }
}
