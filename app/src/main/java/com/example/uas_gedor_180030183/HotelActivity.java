package com.example.uas_gedor_180030183;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.uas_gedor_180030183.data.entity.Hotel;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class HotelActivity extends AppCompatActivity {
    Bitmap bitmap;
    byte img[];
    Button back,tambah;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    SQLiteDatabase db;
    Model hotel;
    ImageView imgview;
    EditText nama,location,price,star;
    private Uri selectedImageUri;
    Button foto;
    private static final int PICK_IMAGE = 1;

    List<Hotel> dataList;
    ListView listviewdata;
    ArrayList<String> records;
    HotelAdapter adapter;
    Cursor tes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel);
        hotel = new Model(this);
        db = hotel.getWritableDatabase();
        hotel.createTable(db);
        hotel.generateHotel(db);

        tes = db.rawQuery("SELECT * FROM hotel",null);
//        System.out.print(tes.getString(1));

        back = (Button) findViewById(R.id.back);
        tambah = (Button) findViewById(R.id.tambah);

        listviewdata= findViewById(R.id.ganteng);
        records=new ArrayList<String>();
        adapter=new HotelAdapter(this,R.layout.activity_list_item,R.id.text_id,records);
        listviewdata.setAdapter(adapter);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HotelActivity.this,AdminActivity.class));
            }
        });
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HotelActivity.this,HotelAdd.class));
            }
        });


    }

    public void selectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                PICK_IMAGE);
    }

    private void DialogForm() {
        hotel = new Model(this);
        db = hotel.getWritableDatabase();
        hotel.createTable(db);

        dialog = new AlertDialog.Builder(HotelActivity.this);
        inflater = getLayoutInflater();

        dialogView = inflater.inflate(R.layout.form_tambah_hotel, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle("Tambah Data");
        foto = (Button) dialogView.findViewById(R.id.foto);

        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageFromGallery();
            }
        });

        imgview = (ImageView) dialogView.findViewById(R.id.imageView);
        nama = (EditText) dialogView.findViewById(R.id.nama_hotel);
        location = (EditText) dialogView.findViewById(R.id.location);
        price = (EditText) dialogView.findViewById(R.id.price);
        star = (EditText) dialogView.findViewById(R.id.kelas);

        dialog.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                ContentValues cv = new ContentValues();



                cv.put("nama", nama.getText().toString());
                cv.put("location",location.getText().toString());
                cv.put("price",price.getText().toString());
                cv.put("star",star.getText().toString());
                cv.put("foto","tessss");

                db.insert("hotel",null,cv);
                Toast.makeText(getApplicationContext(),nama.getText().toString(),Toast.LENGTH_SHORT);
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"Tutup ....".toString(),Toast.LENGTH_SHORT);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        }
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    protected void onActivityResult(int RC, int RQC, Intent I) {

        super.onActivityResult(RC, RQC, I);

        if (RC == PICK_IMAGE) {
            selectedImageUri = I.getData();
            if (null != selectedImageUri) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),

                            selectedImageUri);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG,100,bos);
                    img = bos.toByteArray();

                } catch (IOException er) {
                    er.printStackTrace();
                    Toast.makeText(this, "Ada kesalahan dalam pemilihan gambar", Toast.LENGTH_SHORT).show();;
                }
                imgview.setImageURI(selectedImageUri);
            }
        }
    }

    public static String saveImageToInternalStorage(Bitmap bitmap, Context ctx) {
        ContextWrapper ctxWrapper = new ContextWrapper(ctx);
        File file = ctxWrapper.getDir("images", MODE_PRIVATE);
        String uniqueID = UUID.randomUUID().toString();
        file = new File(file, "hotel-"+ uniqueID + ".jpg");
        try {
            OutputStream stream = null;
            stream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
        } catch (IOException er) {
            er.printStackTrace();
        }

        Uri savedImage = Uri.parse(file.getAbsolutePath());
        return savedImage.toString();
    }

}
