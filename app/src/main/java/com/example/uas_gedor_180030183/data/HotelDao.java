package com.example.uas_gedor_180030183.data;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import com.example.uas_gedor_180030183.data.entity.Hotel;
import io.reactivex.Completable;
import io.reactivex.Single;
import java.util.List;

@Dao
public interface HotelDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertHotel(Hotel hotel);

    @Query("Select * from tb_hotel")
    Single<List<Hotel>> getListHotel();

    @Update
    void updateHotel(Hotel hotel);
}
