package com.example.uas_gedor_180030183.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tb_hotel")
public class Hotel {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "nama")
    private String nama;
    @ColumnInfo(name = "location")
    private String location;
    @ColumnInfo(name = "price")
    private String price;
    @ColumnInfo(name = "star")
    private String star;
    @ColumnInfo(name = "foto")
    private String foto;

    public Hotel (String nama, String location, String price, String star, String foto) {
        this.nama = nama;
        this.location = location;
        this.price = price;
        this.star = star;
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(final String nama) {
        this.nama = nama;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(final String price) {
        this.price = price;
    }

    public String getStar() {
        return star;
    }

    public void setStar(final String star) {
        this.star = star;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(final String foto) {
        this.foto = foto;
    }
}
